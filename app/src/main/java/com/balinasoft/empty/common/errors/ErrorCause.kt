package com.balinasoft.empty.common.errors

import android.app.Activity
import android.content.Context
import android.view.View
import androidx.fragment.app.Fragment
import com.balinasoft.empty.common.errors.ErrorCause.Companion.ERROR_SILENT
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.MvpViewState
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.empty.R
import com.balinasoft.empty.common.extension.showMessageDialog
import com.google.gson.Gson
import retrofit2.HttpException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class BodyError(val message: String)

class ErrorCause : Exception {

    var messageResId: Int? = null

    var httpCode: Int? = null
        private set

    var isConnectException = false

    var params: List<Any>? = null

    companion object {

        const val ERROR_WRONG_ROLE = "ERROR_WRONG_ROLE"
        const val ERROR_UNAUTHORIZED = "ERROR_UNAUTHORIZED"
        const val ERROR_SILENT = "ERROR_SILENT"
        const val ERROR_USER_VERIFICATION = "ERROR_USER_VERIFICATION"

        fun createErrorCause(e: Throwable): ErrorCause = when (e) {
            is ErrorCause -> e
            is SocketTimeoutException -> ErrorCause(R.string.error_unknown_host_exception).also {
                it.isConnectException = true
            }
            is UnknownHostException -> ErrorCause(R.string.error_unknown_host_exception).also {
                it.isConnectException = true
            }
            is ConnectException -> ErrorCause(R.string.error_unknown_host_exception).also {
                it.isConnectException = true
            }
            else -> ErrorCause(R.string.error_unknown)
        }

    }

    fun isUnauthorized() = message == ERROR_UNAUTHORIZED

    constructor(message: String) : super(message)

    constructor(message: String, httpException: HttpException) : super(message, httpException) {
        this.httpCode = httpException.code()
    }

    constructor(httpException: HttpException) : super(httpException) {
        this.httpCode = httpException.code()
    }

    constructor(messageResId: Int) {
        this.messageResId = messageResId
    }

    constructor(messageResId: Int, list: List<Any>) {
        this.messageResId = messageResId
        this.params = list
    }

    fun getErrorCause(context: Context): String {
        val errorMessage = message

        if (errorMessage != null) {
            return errorMessage
        }

        val params = this.params

        if (params != null) {

            return String.format(context.getString(messageResId!!), *params.toTypedArray())
            //val i = params.map { it }


        } else {
            return context.getString(messageResId!!)
        }
    }

}

@StateStrategyType(OneExecutionStateStrategy::class)
fun MvpView.showMessage(errorCause: ErrorCause, reloadable: ((String) -> Unit)? = null) {

    if (errorCause.message == ERROR_SILENT) {
        return
    }

    val view = if (this is MvpViewState<*>) {
        (this as MvpViewState<*>).views.find { it is Context || it is Fragment }
    } else {
        null
    }

    if (errorCause.message == ErrorCause.ERROR_UNAUTHORIZED) {
        when (view) {

        }
    }

    val context: Context? = view?.getContext() ?: this.getContext()
    context?.let {

        val message = errorCause.getErrorCause(it)

        if (reloadable != null) {
            reloadable.invoke(message)
        } else {
            view?.showMessageDialog(message, R.string.continue_str)
        }
    }
}

fun MvpView.getContext(): Context? {
    return when (this) {
        is Activity -> this
        is Fragment -> this.activity
        else -> null
    }
}

fun <T : View> MvpView.getLayout(id: Int): T? {
    return when (this) {
        is Activity -> findViewById(id)
        is Fragment -> view?.findViewById(id)
        else -> null
    }
}

@StateStrategyType(OneExecutionStateStrategy::class)
fun MvpView.showMessage(e: Throwable, reloadable: ((String) -> Unit)? = null) {

    e.printStackTrace()


    val errorMessage = when (e) {
        is ErrorCause -> e
        is HttpException -> {
            when (e.code()) {
                401 -> ErrorCause(ErrorCause.ERROR_UNAUTHORIZED)
                403 -> ErrorCause(ErrorCause.ERROR_UNAUTHORIZED)

                else -> ErrorCause(e.parse().message ?: "Unknown error")

            }

        }
        is SocketTimeoutException -> ErrorCause(R.string.error_socket_timeout_exception)
        is UnknownHostException -> ErrorCause(R.string.error_unknown_host_exception)
        is ConnectException -> ErrorCause(R.string.error_unknown_host_exception)


        else -> {
            val s = e.localizedMessage ?: e.message
            if (s != null) {
                ErrorCause(s)
            } else {
                ErrorCause(R.string.error_unknown)
            }
        }
    }

    this.showMessage(errorMessage, reloadable)
}

fun HttpException.parse(): Exception {
    return try {
        Exception(Gson().fromJson(this.response().errorBody()?.string(), BodyError::class.java).message, this.cause)
    } catch (e: Exception) {
        Exception("Unknown errror")
    }
}
