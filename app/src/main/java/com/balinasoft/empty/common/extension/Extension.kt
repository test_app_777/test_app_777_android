package com.balinasoft.empty.common.extension

import android.app.Activity
import android.content.Context
import android.content.Context.INPUT_METHOD_SERVICE
import android.content.res.Resources
import android.graphics.Color
import android.graphics.Typeface
import android.text.SpannableString
import android.text.Spanned
import android.text.style.StyleSpan
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatSpinner
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

typealias CallBackK<T> = (T) -> Unit
typealias CallBackKUnit = () -> Unit

fun Fragment.setupUI(view: View) {
    activity?.setupUI(view)
}

fun Activity.setupUI(view: View? = this.window.decorView.rootView) {

    if (view != null && !(view is TextView || view is ImageView)) {
        view.requestFocus()
        view.setOnTouchListener { v, _ ->
            v.requestFocus()
            hideSoftKeyboard()
            false
        }
    }

    if (view is ViewGroup) {
        for (i in 0..view.childCount - 1) {
            val innerView = view.getChildAt(i)
            setupUI(innerView)
        }
    }

    view?.requestFocus()

}

fun Activity.hideSoftKeyboard() {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    val view = currentFocus
    if (view != null) {
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }
}

fun Context.showSoftKeyboard(view: View) {
    val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
    view.requestFocus()
    inputMethodManager.showSoftInput(view, 0)
}


/**
 * Convert dp to pixel
 */
val Int.dp: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

/**
 * Convert dp to pixel
 */
val Float.dp: Float
    get() = (this * Resources.getSystem().displayMetrics.density)


fun SwipeRefreshLayout.setRefresh(refresh: Boolean) {
    if (refresh) {
        this.isRefreshing = refresh
    } else {
        this.post { this.isRefreshing = refresh }
    }
}

/*
fun Resources.Theme.isWindowActionBar(): Boolean {
    val typedValue = TypedValue()
    this.resolveAttribute(R.attr.windowActionBar, typedValue, true)
    return typedValue.data == 0
}
*/

fun Resources.getSpannableBoldString(stringId: Int, string: String): SpannableString {
    val spannableString = SpannableString(this.getString(stringId, string))
    spannableString.setSpan(
        StyleSpan(Typeface.BOLD),
        spannableString.indexOf(string),
        spannableString.length,
        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
    )
    return spannableString
}


fun <T> AppCompatSpinner.setAdapterWithPositionWithCustomView(
    list: List<T>, onItemSelected: (item: T, position: Int) -> Unit,
    firstValue: T? = null, isFirstPositionColorGray: Boolean = false,
    layoutId: Int = 0, textViewId: Int
) {
    this.apply {

        val adapterList = kotlin.collections.mutableListOf<T>()
        firstValue?.let {
            adapterList.add(it)
        }
        adapterList.addAll(list)


        adapter = ArrayAdapter<T>(this.context, layoutId, textViewId, adapterList).apply {
            setDropDownViewResource(layoutId)
        }
        onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                onItemSelected.invoke(adapterList[position], position)

                if (isFirstPositionColorGray) {
                    parent?.findViewById<TextView>(textViewId)?.apply {
                        setTextColor(if (position == 0) Color.GRAY else Color.BLACK)
                    }
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

        }
    }
}

