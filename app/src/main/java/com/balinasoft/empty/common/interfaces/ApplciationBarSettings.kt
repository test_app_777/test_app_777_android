package com.balinasoft.empty.common.interfaces

import android.view.View

class ApplicationBarSettings(

    val view: View,
    val showMargin: Boolean

)
