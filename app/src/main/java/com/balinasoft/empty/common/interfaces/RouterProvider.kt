package com.balinasoft.empty.common.interfaces

import ru.terrakok.cicerone.Router

interface RouterProvider {
    fun getRouter(): Router
}
