package com.balinasoft.empty.common.interfaces

interface BackButtonListener {
    fun onBackPressed(): Boolean
}