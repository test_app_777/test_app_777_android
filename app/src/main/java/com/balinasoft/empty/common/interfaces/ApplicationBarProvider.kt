package com.balinasoft.empty.common.interfaces

import android.view.LayoutInflater
import android.view.ViewGroup

interface ApplicationBarProvider {

    fun getApplicationBarLayout(container: ViewGroup, layoutInflater: LayoutInflater): ApplicationBarSettings?

}
