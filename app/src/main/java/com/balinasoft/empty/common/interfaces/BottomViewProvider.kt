package com.balinasoft.empty.common.interfaces

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

interface BottomViewProvider {

    fun getBottomView(parent: ViewGroup, inflater: LayoutInflater): View?

}
