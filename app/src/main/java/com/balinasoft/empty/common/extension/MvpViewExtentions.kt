package com.balinasoft.empty.common.extension

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.balinasoft.empty.common.errors.getContext
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.MvpViewState
import com.balinasoft.empty.R


fun MvpView.showMessageDialog(
    message: String,
    positiveButtonId: Int = R.string.message_dialog_contune,
    onPositiveClicked: (() -> Unit)? = null
) {
    val view = if (this is MvpViewState<*>) {
        (this as MvpViewState<*>).views.find { it is Context || it is Fragment }
    } else {
        null
    }

    val context: Context? = view?.getContext() ?: this.getContext()


    val dialog = AlertDialog.Builder(context!!)
        .setPositiveButton(positiveButtonId, object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                onPositiveClicked?.invoke()
            }
        })
        .create()
    dialog.setMessage(message)
    dialog.show()
}

fun MvpView.showMessageDialog(
    message: String, positiveButtonId: Int,
    onPositiveClicked: (() -> Unit)? = null,
    negativeButton: Int,
    onNegativeClicked: (() -> Unit)? = null
) {

    val view = if (this is MvpViewState<*>) {
        (this as MvpViewState<*>).views.find { it is Context || it is Fragment }
    } else {
        null
    }


    val context: Context? = view?.getContext() ?: this.getContext()

    val dialog = AlertDialog.Builder(context!!)
        .setPositiveButton(positiveButtonId) { _, _ ->
            onPositiveClicked?.invoke()
        }
        .setNegativeButton(negativeButton) { _, _ ->
            onNegativeClicked?.invoke()
        }
        .create()
    dialog.setMessage(message)
    dialog.show()
}
