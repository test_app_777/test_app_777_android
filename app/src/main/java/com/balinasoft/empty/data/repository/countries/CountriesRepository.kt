package com.balinasoft.empty.data.repository.countries

import android.content.Context
import com.balinasoft.empty.data.model.Country
import com.google.gson.GsonBuilder
import java.io.IOException
import java.nio.charset.Charset

class CountriesRepository(val context: Context): ICountriesRepository {

    private fun loadJSONFromAssets(): String? {
        var json: String? = null
        try {
            val inputStream = context.assets?.open("json/test.json")
            val size = inputStream?.available()
            val buffer = ByteArray(size!!)
            inputStream.read(buffer)
            inputStream.close()
            json = String(buffer, Charset.forName("UTF-8"))
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    override fun getCountries(): List<Country> {
        val temp = loadJSONFromAssets()
        return if (temp == null) {
            arrayListOf()
        } else {
            val gson = GsonBuilder().create()
            val countries = gson.fromJson(temp, Array<Country>::class.java)
            countries.asList()
        }
    }
}