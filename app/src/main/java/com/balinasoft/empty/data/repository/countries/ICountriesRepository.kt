package com.balinasoft.empty.data.repository.countries

import com.balinasoft.empty.data.model.Country

interface ICountriesRepository {

    fun getCountries(): List<Country>

}