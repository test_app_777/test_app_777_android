package com.balinasoft.empty.data.model

import com.google.gson.annotations.SerializedName

data class Country(
    @SerializedName("Id") val id: String,
    @SerializedName("Time") val time: String,
    @SerializedName("Name") val name: String,
    @SerializedName("Image") val image: String?
)