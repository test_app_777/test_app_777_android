package com.balinasoft.empty.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class AbstractViewHolder<in T>(parent: ViewGroup, idLayout: Int) :
    RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(idLayout, parent, false)) {
    open fun onBindViewHolder(item: T) {

    }

    open fun onBindViewHolder() {

    }
}
