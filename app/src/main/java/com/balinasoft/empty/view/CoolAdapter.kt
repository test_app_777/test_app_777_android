package com.balinasoft.empty.view

import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import java.util.*
import kotlin.collections.ArrayList


abstract class CoolAdapter : RecyclerView.Adapter<AbstractViewHolder<Any>>() {

    val data = ArrayList<Any>()
    private val mCellInfoMap = Hashtable<Class<out Any>, CellInfo>()


    protected fun registerCell(objectClass: Class<out Any>, @LayoutRes layoutId: Int, bindInterface: BindInterface) {
        val cellInfo = CellInfo(bindInterface, layoutId)
        mCellInfoMap[objectClass] = cellInfo
    }

    init {

    }

    interface BindInterface {
        fun createViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder<Any>
        //fun onBindView(holder: RecyclerView.ViewHolder, obj: Any)
    }

    class CellInfo(
        val bindInterface: BindInterface,
        val layoutId: Int

    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder<Any> {

        val cellInfo = mCellInfoMap.entries.find { it -> it.value.layoutId == viewType }

        cellInfo?.let {
            return cellInfo.value.bindInterface.createViewHolder(parent, viewType)
        }
        throw Exception("No view holder found")
    }


    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: AbstractViewHolder<Any>, position: Int) {
        val item = getItemAt(position)
        holder.onBindViewHolder(item)
    }

    protected fun getItemAt(position: Int): Any {
        return data.get(position)
    }


    protected fun getCellInfo(someModel: Any): CellInfo? {
        for (entry in mCellInfoMap.entries) {
            if (entry.key.isInstance(someModel))
                return entry.value
        }
        return null
    }

    override fun getItemViewType(position: Int): Int {
        return getCellInfo(data[position])!!.layoutId
    }

    open fun insertData(list: List<Any>) {
        data.clear()
        data.addAll(list)
        notifyDataSetChanged()
    }

    open fun addData(list: List<Any>) {
        data.addAll(list)
        notifyDataSetChanged()
    }

    open fun clear() {
        data.clear()
        notifyDataSetChanged()
    }

    fun updateItem(item: Any) {
        val indexToUpdate = data.indexOf(item)
        notifyItemChanged(indexToUpdate)
    }

    open fun remove(i: Int) {
        data.removeAt(i)
        notifyItemRemoved(i)
    }

    open fun remove(item: Any) {
        val index = data.indexOf(item)
        remove(index)
    }

    open fun addItem(item: Any) {
        data.add(item)
        notifyItemInserted(data.size - 1)
    }

    open fun addItem(item: Any, position: Int) {
        data.add(position, item)
    }
}
