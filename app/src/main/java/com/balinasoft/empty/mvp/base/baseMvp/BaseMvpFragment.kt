package com.balinasoft.empty.mvp.base.baseMvp

import android.animation.Animator
import android.animation.ObjectAnimator
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.balinasoft.empty.R
import com.balinasoft.empty.common.interfaces.BackButtonListener
import com.balinasoft.empty.common.interfaces.RouterProvider
import com.balinasoft.empty.common.views.animation.XFractionProperty
import com.balinasoft.empty.mvp.base.baseFragment.BaseFragment
import com.balinasoft.empty.mvp.base.placeholderFragment.IPlaceholderView
import ru.terrakok.cicerone.Router
import java.io.File


abstract class BaseMvpFragment<VIEW : BaseMvpView, PRESENTER : BaseMvpPresenter<VIEW>> : BaseFragment(),
    IPlaceholderView, BackButtonListener {

    protected var tranlationAnimationEnabled: Boolean = true

    fun isAnimationEnabeld(): Boolean {
        return tranlationAnimationEnabled
    }

    open lateinit var presenter: PRESENTER

    companion object {
        val SHOW_ANIM = "SHOW_ANIM"
        val PREW_LEWEL = "PREW_LEWEL"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }

    fun getLevel(): Int {
        val level = arguments?.getInt(PREW_LEWEL, 0) ?: 0
        val myLevel = level + 1
        return myLevel
    }

    public fun getRouter(): Router? {

        parentFragment?.let {
            if (it is RouterProvider)
                return it.getRouter()
        }
        activity?.let {
            if (it is RouterProvider) {
                return it.getRouter()
            }
        }

        return null
    }

    private fun isNeedToShowAnimation(): Boolean {
        return arguments?.getBoolean(SHOW_ANIM, false) ?: false
    }

    override fun onBackPressed(): Boolean {
        getRouter()?.exit()
        return true
    }


    override fun onCreateAnimator(transit: Int, enter: Boolean, nextAnim: Int): Animator? {

        val animator = when (nextAnim) {
            R.animator.slide_in_left -> ObjectAnimator.ofFloat(null, XFractionProperty(), 1f, 0f)
            R.animator.slide_out_right -> ObjectAnimator.ofFloat(null, XFractionProperty(), 0f, -1f)
            R.animator.slide_in_left_b -> ObjectAnimator.ofFloat(null, XFractionProperty(), -1f, 0f)
            R.animator.slide_in_right_b -> ObjectAnimator.ofFloat(null, XFractionProperty(), 0f, 1f)
            else -> null
        }

        animator?.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {

            }

            override fun onAnimationEnd(animation: Animator?) {
                onAnimationEnded()
            }

            override fun onAnimationCancel(animation: Animator?) {
                onAnimationEnded()
            }

            override fun onAnimationStart(animation: Animator?) {

            }
        })
        if (animator == null) {
            onAnimationEnded()
        }

        return animator
    }


    open fun onAnimationEnded() {
        presenter.onAnimationEnded()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    open fun onPhotoSelected(files: MutableList<File>) {

    }
}
