package com.balinasoft.empty.mvp.base.baseScreen.applicationBar

import android.view.View
import com.balinasoft.empty.common.extension.CallBackKUnit

interface ApplicationBarViewHolder {
    fun setBackButtonVisibility(visible: Boolean)
    fun setText(text: String)
    fun getView(): View
    var onBackButtonClicked: CallBackKUnit
}
