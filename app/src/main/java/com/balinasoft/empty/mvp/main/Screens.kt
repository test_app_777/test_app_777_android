package com.balinasoft.empty.mvp.main

import androidx.fragment.app.Fragment
import com.balinasoft.empty.mvp.screens.list.ListFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class Screens {

    class ListScreen() : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return ListFragment()
        }
    }

}
