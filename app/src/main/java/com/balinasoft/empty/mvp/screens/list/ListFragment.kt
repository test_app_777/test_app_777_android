package com.balinasoft.empty.mvp.screens.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.empty.R
import com.balinasoft.empty.data.model.Country
import com.balinasoft.empty.mvp.base.baseScreen.BaseScreenFragment
import com.balinasoft.empty.mvp.screens.list.adapter.CountryAdapter
import com.balinasoft.runes.ui.views.ListPaddingDecoration
import kotlinx.android.synthetic.main.fragment_list.*
import org.jetbrains.anko.support.v4.dimen

class ListFragment: BaseScreenFragment<ListView, ListPresenter>(), ListView {

    @InjectPresenter
    override lateinit var presenter: ListPresenter

    @ProvidePresenter
    fun providePresenter() = ListPresenter()

    private val adapter = CountryAdapter(::onCountryClick, ::onLongCountryClick)

    override fun provideYourView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    override fun initViews(countries: List<Country>) {
        adapter.insertData(countries)
        countriesRV.adapter = adapter
        countriesRV.layoutManager = LinearLayoutManager(context)
        countriesRV.addItemDecoration(ListPaddingDecoration(context!!, dimen(R.dimen.padding_4), dimen(R.dimen.padding_4)))
    }

    private fun onCountryClick(country: Country, position: Int) {

    }

    private fun onLongCountryClick(country: Country, position: Int) {
        adapter.remove(position)
    }
}