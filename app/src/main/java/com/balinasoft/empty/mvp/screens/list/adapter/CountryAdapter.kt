package com.balinasoft.empty.mvp.screens.list.adapter

import android.view.ViewGroup
import com.balinasoft.empty.data.model.Country
import com.balinasoft.empty.view.AbstractViewHolder
import com.balinasoft.empty.view.CoolAdapter

class CountryAdapter (val callback: (country: Country, position: Int) -> Unit, val callbackLong: (country: Country, position: Int) -> Unit) : CoolAdapter() {

    init {
        registerCell(Country::class.java, CountryViewHolder.VIEW_TYPE, object : BindInterface {
            override fun createViewHolder(
                parent: ViewGroup,
                viewType: Int
            ): AbstractViewHolder<Any> {
                return CountryViewHolder(parent, callback, callbackLong) as AbstractViewHolder<Any>
            }
        })
    }
}