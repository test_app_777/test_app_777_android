package com.balinasoft.empty.mvp.screens.list

import com.arellomobile.mvp.InjectViewState
import com.balinasoft.empty.di.DaggerUtils
import com.balinasoft.empty.mvp.base.baseScreen.BaseScreenPresenter

@InjectViewState
class ListPresenter  : BaseScreenPresenter<ListView>() {

    private val countriesRepository = DaggerUtils.appComponent.provideCountriesRepository()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.initViews(countriesRepository.getCountries())
    }


}