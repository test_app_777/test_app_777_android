package com.balinasoft.empty.mvp.screens.list.adapter

import android.view.ViewGroup
import com.balinasoft.empty.R
import com.balinasoft.empty.common.utils.ImageLoader
import com.balinasoft.empty.data.model.Country
import com.balinasoft.empty.view.AbstractViewHolder
import kotlinx.android.synthetic.main.item_country.view.*
import com.balinasoft.empty.common.extension.onClick
import com.balinasoft.empty.common.extension.onLongClick

class CountryViewHolder (
    parent: ViewGroup,
    private val callback: (country: Country, position: Int) -> Unit,
    private val callbackLong: (country: Country, position: Int) -> Unit
) :
    AbstractViewHolder<Country>(parent, VIEW_TYPE) {

    companion object {
        const val VIEW_TYPE = R.layout.item_country
    }

    override fun onBindViewHolder(item: Country) {
        super.onBindViewHolder(item)

        with(itemView) {

            if (item.image == null) {

            } else {
                ImageLoader.load(item.image, imageView)
            }

            textView.text = item.name

            onClick {
                callback.invoke(item, adapterPosition)
            }

            onLongClick {
                callbackLong.invoke(item, adapterPosition)
                true
            }
        }
    }
}
