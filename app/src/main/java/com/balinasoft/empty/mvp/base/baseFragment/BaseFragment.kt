package com.balinasoft.empty.mvp.base.baseFragment

import android.os.Bundle
import android.os.Parcelable
import com.arellomobile.mvp.MvpAppCompatFragment


open class BaseFragment() : MvpAppCompatFragment() {

    companion object {
        val ARG = "BASEFRAGMENTARG"
    }

    fun <T : Parcelable> putParcelableArg(arg: T) {
        arguments = (arguments ?: Bundle()).also {
            it.putParcelable(ARG, arg)
        }
    }

    fun <T : Parcelable> getParcelableArg(): T? {
        return arguments?.getParcelable<T>(ARG)
    }

}
