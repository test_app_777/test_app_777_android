package com.balinasoft.empty.mvp.base.baseMvp

import com.arellomobile.mvp.MvpPresenter

open class BaseMvpPresenter<VIEW : BaseMvpView> : MvpPresenter<VIEW>() {

    private var isFirstLaunch: Boolean = true

    fun onAnimationEnded() {

        afterAnimation()

        if (isFirstLaunch) {
            onFirstAttachAfterAnimation()
            isFirstLaunch = false
        }

    }


    open fun onFirstAttachAfterAnimation() {}

    open fun afterAnimation() {}


}