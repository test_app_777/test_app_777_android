package com.balinasoft.empty.mvp.main.tabactivity

interface AppBarContainer {

    fun invalidateApplicationBar()
    fun invalidateBottomView()


}
