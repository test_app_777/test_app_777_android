package com.balinasoft.empty.mvp.base.placeholderFragment

import com.balinasoft.empty.mvp.base.baseMvp.BaseMvpPresenter

open class PlaceholderPresenter<VIEW : IPlaceholderView> : BaseMvpPresenter<VIEW>() {


}