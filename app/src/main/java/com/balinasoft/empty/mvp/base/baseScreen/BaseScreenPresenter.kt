package com.balinasoft.empty.mvp.base.baseScreen

import com.balinasoft.empty.mvp.base.placeholderFragment.PlaceholderPresenter
import ru.terrakok.cicerone.Router

abstract class BaseScreenPresenter<NEW : BaseScreenView> : PlaceholderPresenter<NEW>() {

    var router: Router? = null

    open fun refresh():Boolean {
        return false
    }

    open fun onViewCreatedFirstTime() {}

}
