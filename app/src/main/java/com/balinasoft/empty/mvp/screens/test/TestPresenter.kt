package com.balinasoft.empty.mvp.screens.test

import com.arellomobile.mvp.InjectViewState
import com.balinasoft.empty.mvp.base.baseScreen.BaseScreenPresenter

@InjectViewState
class TestPresenter : BaseScreenPresenter<TestView>() {
}