package com.balinasoft.empty.mvp.base.activity

import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import com.balinasoft.empty.R
import com.balinasoft.empty.common.interfaces.BackButtonListener
import com.balinasoft.empty.common.interfaces.RouterProvider
import com.balinasoft.empty.di.DaggerUtils
import com.balinasoft.empty.mvp.main.Screens
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Replace
import javax.inject.Inject

class RootActivity : MvpAppCompatActivity(), RouterProvider {

    var timeOfBeginPause = 0L

    override fun getRouter(): Router {
        return router_m
    }

    var navigatorHolder: NavigatorHolder = DaggerUtils.appComponent.provideNavigatorHolder()

    var navigator: SupportAppNavigator? = null
        get() {
            var nav = field
            if (nav == null) {
                nav = SupportAppNavigator(this, supportFragmentManager, R.id.ftc_container_root_activity)
                field = nav
            }
            return nav
        }

    @Inject
    lateinit var router_m: Router


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerUtils.appComponent.inject(this)

        setContentView(R.layout.activity_root_activity)

        if (savedInstanceState == null) {
            navigator?.applyCommands(arrayOf<Command>(Replace(Screens.ListScreen())))
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
        timeOfBeginPause = System.currentTimeMillis()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.ftc_container_root_activity)
        if (fragment != null
            && fragment is BackButtonListener
            && (fragment as BackButtonListener).onBackPressed()
        ) {
            return
        } else {
            (this as RouterProvider).getRouter().exit()
            return
        }
    }
}
