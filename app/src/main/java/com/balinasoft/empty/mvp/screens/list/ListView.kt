package com.balinasoft.empty.mvp.screens.list

import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.empty.data.model.Country
import com.balinasoft.empty.mvp.base.baseScreen.BaseScreenView

interface ListView: BaseScreenView {

    @StateStrategyType(AddToEndStrategy::class)
    fun initViews(countries: List<Country>)

}