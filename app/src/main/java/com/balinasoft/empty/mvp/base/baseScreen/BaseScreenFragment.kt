package com.balinasoft.empty.mvp.base.baseScreen

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.balinasoft.empty.R
import com.balinasoft.empty.common.interfaces.ApplicationBarProvider
import com.balinasoft.empty.common.interfaces.ApplicationBarSettings
import com.balinasoft.empty.common.interfaces.BottomViewProvider
import com.balinasoft.empty.mvp.base.baseScreen.applicationBar.ApplicationBarViewHolder
import com.balinasoft.empty.mvp.base.placeholderFragment.PlaceholderFragment
import com.balinasoft.empty.mvp.main.tabactivity.AppBarContainer
import kotlinx.android.synthetic.main.placeholder_error.view.*
import com.balinasoft.empty.common.extension.onClick

abstract class BaseScreenFragment<VIEW : BaseScreenView, PRESENTER : BaseScreenPresenter<VIEW>> :
    PlaceholderFragment<VIEW, PRESENTER>(), BaseScreenView, ApplicationBarProvider, BottomViewProvider {

    override lateinit var presenter: PRESENTER

    private lateinit var noInternet: View
    private lateinit var progressBar: View
    private var applicationBarText: String? = null

    private var whereShowApplicationBar: PlaceToShowApplicationBar = PlaceToShowApplicationBar.IN_FRAGMENT

    private var bottomView: View? = null

    protected fun setBottomView(view: View?) {
        this.bottomView = view
        invalidateBottomView()
    }

    private fun invalidateBottomView() {
        val a = this.activity
        if (a is AppBarContainer) {
            a.invalidateBottomView()
        }
    }

    override fun getBottomView(parent: ViewGroup, inflater: LayoutInflater): View? {
        return bottomView
    }

    protected fun setApplicationBarLocation(location: PlaceToShowApplicationBar) {
        this.whereShowApplicationBar = location
    }

    class MyLayout(
        val id: Int,
        val view: View,
        var visible: Boolean
    )

    enum class PlaceToShowApplicationBar() {

        IN_FRAGMENT, IN_ACTIVITY

    }

    private var inited: Boolean = false

    private val layoutList = ArrayList<MyLayout>()

    var applicationBarViewHolder: ApplicationBarViewHolder? = null

    override fun buildApplicationBar(inflater: LayoutInflater, container: ViewGroup): ApplicationBarView? {
        if (whereShowApplicationBar != PlaceToShowApplicationBar.IN_FRAGMENT) {
            return null
        }

        applicationBarViewHolder = getApplicationBarViewHolder(inflater, container)
        applicationBarViewHolder?.let {

            it.setBackButtonVisibility(needToShowBackButton())
            it.onBackButtonClicked = {
                exit()
            }
            it.setText(applicationBarText?:"")
            return ApplicationBarView(it.getView(), true)
        }

        return null
    }

    override fun getApplicationBarLayout(
        container: ViewGroup,
        layoutInflater: LayoutInflater
    ): ApplicationBarSettings? {

        if (whereShowApplicationBar != PlaceToShowApplicationBar.IN_ACTIVITY) {
            return null
        }

        applicationBarViewHolder = getApplicationBarViewHolder(layoutInflater, container)

        applicationBarViewHolder?.let {

            it.setBackButtonVisibility(needToShowBackButton())
            it.onBackButtonClicked = {
                exit()
            }
            it.setText(applicationBarText ?: "")

            val view = ApplicationBarView(it.getView(), true)

            return ApplicationBarSettings(view.view, view.showMargin)
        }
        return null
    }

    private fun firstInit() {
        presenter.router = getRouter()

        noInternet = buildView(R.layout.placeholder_error).also {
            it.reloadButton.onClick {
                refresh()
            }
        }

        progressBar = buildView(R.layout.placeholder_progress_bar)

        layoutList.add(MyLayout(R.id.no_internet_placeholder, noInternet, false))
        layoutList.add(MyLayout(R.id.progress_bar, progressBar, false))

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!inited) {
            firstInit()
        }

        inited = true
        updateView()

    }

    override fun onStart() {
        super.onStart()
        val activity = this.activity
        if (activity is AppBarContainer) {
            Log.d("TAB_DEBUG", "call activity invalidate: ${this.toString()} PARENT: ${parentFragment?.toString()}")
            activity.invalidateApplicationBar()
            activity.invalidateBottomView()
        }
    }

    //--------------------PUBLIC FUNCTIONS -----------------------

    override fun showNoInternetPlaceholder(show: Boolean, text: String) {
        setViewFlag(R.id.no_internet_placeholder, show)
        noInternet.errorMessageText.text = text
    }

    override fun showProgressBar(show: Boolean) {
        setViewFlagWithoutUpdateView(R.id.no_internet_placeholder, false)
        setViewFlagWithoutUpdateView(R.id.progress_bar, show)
        updateView()
    }

    override fun setApplicationBarTitle(title: String) {
        applicationBarText = title
        applicationBarViewHolder?.setText(title)
    }

    //--------------------FUNCTIONS TO OVERRIDE-----------------------

    open fun refresh() {
        presenter.refresh()
    }

    open fun getApplicationBarViewHolder(
        layoutInflater: LayoutInflater,
        container: ViewGroup
    ): ApplicationBarViewHolder? {
        return null
    }

    open fun needToShowBackButton(): Boolean {
        return !isRootFragment()
    }

    open fun exit(){
        getRouter()?.exit()
    }

    //--------------------PRIVATE FUNCTIONS-----------------------

    private fun setViewFlag(id: Int, show: Boolean) {
        layoutList.find { it.id == id }?.visible = show
        updateView()
    }

    private fun setViewFlagWithoutUpdateView(id: Int, show: Boolean) {
        layoutList.find { it.id == id }?.visible = show
    }

    private fun updateView() {
        val view = layoutList.findLast { it.visible }
        applyView(view)
    }

    private fun applyView(view: MyLayout?) {
        if (view == null) {
            showPlaceholder(false)
        } else {
            showPlaceholder(true)
            setPlaceholder(view.view)
        }
    }

    protected fun isRootFragment(): Boolean {
        val level = getLevel()
        return level == 1
    }
}


