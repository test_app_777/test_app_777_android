package com.balinasoft.empty.mvp.base.baseScreen.applicationBar

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.balinasoft.empty.R
import kotlinx.android.synthetic.main.view_application_bar.view.*
import com.balinasoft.empty.common.extension.CallBackKUnit
import com.balinasoft.empty.common.extension.onClick
import com.balinasoft.empty.common.extension.visibleOrGone

class StandartApplicationBarViewHolder(layoutInflater: LayoutInflater, container: ViewGroup) :
    ApplicationBarViewHolder {

    private val view: View = layoutInflater.inflate(R.layout.view_application_bar, container, false)

    override var onBackButtonClicked: CallBackKUnit = {}

    init {
        view.backButton.onClick {
            onBackButtonClicked.invoke()
        }

    }

    override fun setBackButtonVisibility(visible: Boolean) {
        view.backButton.visibleOrGone(visible)
    }

    override fun setText(text: String) {
        view.toolbarTextView.text = text
    }

    override fun getView(): View {
        return view
    }
}
