package com.balinasoft.empty.mvp.base.baseScreen

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.empty.mvp.base.placeholderFragment.IPlaceholderView

interface BaseScreenView : IPlaceholderView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showNoInternetPlaceholder(show: Boolean, text: String)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showProgressBar(show: Boolean)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setApplicationBarTitle(title: String)

}
