package com.balinasoft.empty.mvp.screens.test

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.empty.R
import com.balinasoft.empty.mvp.base.baseScreen.BaseScreenFragment

class TestFragment: BaseScreenFragment<TestView, TestPresenter>(), TestView {

    @InjectPresenter
    override lateinit var presenter: TestPresenter

    @ProvidePresenter
    fun providePresenter() = TestPresenter()

    override fun provideYourView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_test, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

}