package com.balinasoft.empty.mvp.base.placeholderFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.balinasoft.empty.R
import com.balinasoft.empty.mvp.base.baseMvp.BaseMvpFragment
import kotlinx.android.synthetic.main.fragment_base_placeholder_view.*
import kotlinx.android.synthetic.main.fragment_base_placeholder_view.view.*
import com.balinasoft.empty.common.extension.visibleOrInvisible

abstract class PlaceholderFragment<VIEW : IPlaceholderView, PRESENTER : PlaceholderPresenter<VIEW>> :

    BaseMvpFragment<VIEW, PRESENTER>(), IPlaceholderView {

    protected var showShadowBetweenApplicationBarAndContent = false
        set(value) {
            field = value
            applicationBarShadow?.visibleOrInvisible(value)
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_base_placeholder_view, container, false)

        val viewToAttach = provideYourView(inflater, view.payloadContainer, savedInstanceState)
        view.payloadContainer.addView(viewToAttach)

        val applicationBar = buildApplicationBar(inflater, view.applicationBarContainer)

        if (applicationBar != null) {
            view.applicationBarContainer.addView(applicationBar.view)
            view.applicationBarShadow.visibleOrInvisible(applicationBar.showShadow)
        }

        val header = getHeader(inflater,view.headerContainer)
        header?.let {
            view.headerContainer.addView(it)
        }

        view.applicationBarShadow.visibleOrInvisible(showShadowBetweenApplicationBarAndContent)

        return view
    }

    abstract fun provideYourView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View

    protected fun setPlaceholder(view: View) {
        placeholderContainer.removeAllViews()
        if (view.parent != null) {
            val i = 0
        } else {
            placeholderContainer.addView(view)
        }

    }

    protected fun buildView(id: Int): View {
        return layoutInflater.inflate(id, placeholderContainer, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showPlaceholder(false)
    }

    protected fun showPlaceholder(show: Boolean) {
        placeholderContainer.visibleOrInvisible(show)
        payloadContainer.visibleOrInvisible(!show)
    }

    class ApplicationBarView(
        val view: View,
        val showShadow: Boolean
    ) {
        val showMargin: Boolean = true
    }

    open fun buildApplicationBar(inflater: LayoutInflater, container: ViewGroup): ApplicationBarView? {
        return null
    }

    open fun getHeader(inflater: LayoutInflater, container: ViewGroup?):View?{
        return null
    }
}
