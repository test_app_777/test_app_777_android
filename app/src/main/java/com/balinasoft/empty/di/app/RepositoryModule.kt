package com.balinasoft.empty.di.app

import android.content.Context
import com.balinasoft.empty.data.repository.countries.CountriesRepository
import com.balinasoft.empty.data.repository.countries.ICountriesRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideCountriesRepository(): ICountriesRepository = CountriesRepository(context)

}