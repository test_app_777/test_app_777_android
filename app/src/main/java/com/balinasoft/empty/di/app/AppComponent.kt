package com.balinasoft.empty.di.app

import com.balinasoft.empty.data.repository.countries.ICountriesRepository
import com.balinasoft.empty.mvp.base.activity.RootActivity
import dagger.Component
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        LocalNavigationModule::class,
        RepositoryModule::class]
)
interface AppComponent {

    fun provideNavigatorHolder(): NavigatorHolder

    fun provideGlobalRouter(): Router

    fun provideCountriesRepository(): ICountriesRepository

    fun inject(rootActivity: RootActivity)
}