package com.balinasoft.empty.di

import android.content.Context
import com.balinasoft.empty.di.app.AppComponent
import com.balinasoft.empty.di.app.AppModule
import com.balinasoft.empty.di.app.DaggerAppComponent
import com.balinasoft.empty.di.app.RepositoryModule

object DaggerUtils {

    lateinit var appComponent: AppComponent
    //var menuComponent: MenuComponent? = null
    //var checkoutComponent: CheckoutComponent? = null

    //lateinit var navigatorActivityComponent: NavigatorActivityComponent

    /*fun initMenuComponent(): MenuComponent {
        appComponent.provideMenuComponent().let {
            menuComponent = it
            return it
        }
    }
*/
    /*fun initCheckoutComponent(): CheckoutComponent {
        appComponent.provideCheckoutComponent().let {
            checkoutComponent = it
            return it
        }
    }*/

    /* fun destroyMenComponent() {
         menuComponent = null
     }

     fun destroyCheckoutComponent() {
         checkoutComponent = null
     }
 */

    @JvmStatic
    fun buildComponents(context: Context) {

        appComponent = buildAppComponent(context)
        // navigatorActivityComponent = buildNavigatorActivityComponent()
    }

    private fun buildAppComponent(context: Context) = DaggerAppComponent.builder()
        .appModule(AppModule())
        .repositoryModule(RepositoryModule(context))
        .build()

    /*private fun buildNavigatorActivityComponent() = DaggerNavigatorActivityComponent
        .builder()
        .appComponent(appComponent)
        .build()*/
}