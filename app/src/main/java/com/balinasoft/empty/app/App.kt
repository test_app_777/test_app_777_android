package com.balinasoft.empty.app

import android.content.Context
import androidx.multidex.MultiDexApplication
import com.balinasoft.empty.di.DaggerUtils

class App() : MultiDexApplication() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        DaggerUtils.buildComponents(this)
    }
}
